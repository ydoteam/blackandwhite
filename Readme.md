#blackAndWhite.js#

A javascript function which intelligently changes color of foreground elements based on background. It selects between black and white.

##Invocation##
The funtion can be invocated at any point after the background and foreground elements and the script itself are loaded. It takes simple css selectors as parameters. Both parameters are required; backround is taken fist. `blackAndWhite("div.ex-bg","p.ex-fg");`

##jQuery##
This script requires jQuery. It can be loaded via CDN or Locally. It can be found in the repository.

##Example##
From `test.htm`:

	<HTMl>
		<head>
			<title>Test Page</title>
			<script src="jquery.min.js"></script>
			<script src="blackAndWhite.js"></script>
			<style>
				div.ex-bg{
					background-color: red;
				}
				p.ex-fg{
					color: blue;
				}
			</style>
		</head>
		<body>
			<div class="ex-bg">
				<p class="ex-fg">Test Text</p>
			</div>
			<script>
				blackAndWhite("div.ex-bg","p.ex-fg");
			</script>
		</body>
	</HTML>